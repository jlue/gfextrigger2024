#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/ToolHandle.h>
#include <TrigDecisionTool/TrigDecisionTool.h>





class MyxAODAnalysis : public EL::AnaAlgorithm
{
public:
  /// This is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  /// These are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

 std::string m_trigDecTool_name{"TDT:JetTriggerEfficiencies"};



private:
  /// Configuration, and any other types of variables go here.
  //
  int m_runNumber;
  int m_eventNumber;

  ToolHandle<Trig::TrigDecisionTool> m_trigDecTool;
  
  // try adding this of list of name
  // borrowing this from AthMonitorAlgorithm
  Gaudi::Property<std::string> m_triggerChainString {this,"TriggerChain","","comma separated list of chains"}; ///< Trigger chain string pulled from the job option and parsed into a vector
  std::vector<std::string> m_vTrigChainNames; ///< Vector of trigger chain names parsed from trigger chain string
  StatusCode parseList( const std::string& line, std::vector<std::string>& result ) const;
  void unpackTriggerCategories( std::vector<std::string>& vTrigChainNames ) const;


};

#endif
