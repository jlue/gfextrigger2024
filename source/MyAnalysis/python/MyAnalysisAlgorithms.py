from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createService

# Add configurations here

triggerChains = [
    'HLT_e26_lhtight_nod0_ivarloose',
    'HLT_e60_lhmedium',
    'HLT_mu26_ivarmedium',
    'HLT_mu50'
]


def makeSequence (dataType) :

    algSeq = AlgSequence()

    # Set up CommonServiceSequence
    from AsgAnalysisAlgorithms.CommonServiceSequence import makeCommonServiceSequence
    makeCommonServiceSequence (algSeq, runSystematics = True)

    # Add your sequences here
    
    # Include and set up the trigger analysis sequence:
    from TriggerAnalysisAlgorithms.TriggerAnalysisSequence import makeTriggerAnalysisSequence
    triggerSequence = makeTriggerAnalysisSequence( dataType, triggerChains=triggerChains, noFilter=False )

    # Add the trigger analysis sequence to the job:
    algSeq += triggerSequence

    return algSeq

    # Do not add anything here!!!
