#include <TH1.h>
#include <xAODJet/JetContainer.h>
#include <xAODTrigger/JetRoIContainer.h>
#include "xAODTrigger/JetRoIAuxContainer.h"
#include "xAODTrigger/JetRoIContainer.h"
#include "xAODTrigger/JetRoI.h"

// maybe need to add some more ?  https://gitlab.cern.ch/atlas-trigger/jet/jetTriggerEfficiencies/-/blob/R22-master/Root/JetTriggerEfficiencies.cxx?ref_type=heads

#include "xAODTrigger/jFexSRJetRoIContainer.h"
#include "xAODTrigger/jFexLRJetRoIContainer.h"
#include "xAODTrigger/gFexJetRoIContainer.h"

#include <xAODTrigger/gFexJetRoIContainer.h>





#include <AsgMessaging/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>

#include "TrigDecisionTool/TrigDecisionTool.h"

#include <xAODEventInfo/EventInfo.h>
//#include <xAODCaloEvent/CaloClusterContainer.h>




MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),    
      m_trigDecTool( "TrigDecisionTool/tdt", this ),
      m_vTrigChainNames({})
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. This is also where you
  // declare all properties for your algorithm. Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

 declareProperty ("triggerDecTool", m_trigDecTool, "the trigger decision tool");

}



StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees. This method gets called before any input files are
  // connected.
  //

  // Create output tree                                                                                                                
  ANA_CHECK(book(TTree("analysis", "My analysis ntuple")));                                                                            
  TTree *mytree = tree("analysis");     

  // Declare branches                                                                                                                  
  mytree->Branch("RunNumber", &m_runNumber);                                                                                           
  mytree->Branch("EventNumber", &m_eventNumber); 

  ANA_MSG_INFO ("DMS in initialize");
  ANA_CHECK (book (TH1F ("h_gjetPt", "h_gjetPt max ; [GeV] ", 150, 0, 1500))); // jet pt [GeV]              
  ANA_CHECK (book (TH1F ("h_gjetEta", "h_gjetEta max", 50, -5.0, 5.0)));
  ANA_CHECK (book (TH1F ("h_gjetPhi", "h_gJetPhi max", 34, -3.14159-0.15, 3.14159+0.25)));


   // handle doesn't exist?  
   ANA_CHECK( m_trigDecTool.retrieve());
   if ( !m_trigDecTool.empty() ) {
      ANA_CHECK( m_trigDecTool.retrieve() );
      ANA_MSG_DEBUG( "TDT retrieved" );
  
    // If the trigger chain is specified, parse it into a list.
    if ( m_triggerChainString!="" ) {
      StatusCode sc = parseList(m_triggerChainString,m_vTrigChainNames);
      // print outs like this cause trouble for AnalysisBase
      ANA_MSG_DEBUG("DMS in parse list"<<m_triggerChainString);
      ANA_MSG_DEBUG("DMS out parse list"<<m_vTrigChainNames);
      if ( !sc.isSuccess() ) {
	ANA_MSG_WARNING("Error parsing trigger chain list, using empty list instead.");
	m_vTrigChainNames.clear();
      }      
    }
   }
   

	return StatusCode::SUCCESS;
}


StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees. This is where most of your actual analysis
  // code will go.

  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() <<
                              ", eventNumber = " << eventInfo->eventNumber() );
  //bool j450 = 0; 
  // bool j450  = eventInfo->auxdata< bool >("trigPassed_HLT_j460_a10r_L1J100");
  //eventInfo->auxdata< bool >("trigPassed_HLT_e26_lhtight_nod0_ivarloose");

  //bool tutorial; 
  //tutorial = eventInfo->auxdata< bool >("trigPassed_HLT_e26_lhtight_nod0_ivarloose");

  //ANA_MSG_INFO ("DMS j450: " << j450 << "tutorial " << tutorial ); 	

 
  bool j450 = m_trigDecTool->isPassed("HLT_j460_a10r_L1J100");

  ANA_MSG_INFO ("DMS j450: " << j450 );    

  bool myL1_gJ20p0ETA25 = m_trigDecTool->isPassed("L1_gJ20p0ETA25");
  ANA_MSG_INFO ("DMS : L1_gJ20p0ETA25: " << myL1_gJ20p0ETA25 );

  bool myL1_gLJ100p0ETA25 = m_trigDecTool->isPassed("L1_gLJ100p0ETA25");
  ANA_MSG_INFO ("DMS : L1_gLJ100p0ETA25: " << myL1_gLJ100p0ETA25 );

  bool myL1_gJ160p0ETA25 = m_trigDecTool->isPassed("L1_gLJ160p0ETA25");
  ANA_MSG_INFO ("DMS : L1_gJ160p0ETA25: " << myL1_gJ160p0ETA25);

  bool myL1_J100 = m_trigDecTool->isPassed("L1_J100");
  ANA_MSG_INFO ("DMS : L1_J100: " << myL1_J100 );

  bool myL1_jJ160 = m_trigDecTool->isPassed("L1_jJ160");
  ANA_MSG_INFO ("DMS : L1_gJ160: " << myL1_jJ160 );

  const xAOD::JetContainer* tjets = nullptr;	
  ANA_CHECK (evtStore()->retrieve (tjets,"InTimeAntiKt4TruthJets"));

  const xAOD::JetContainer* jets = nullptr; 
  ANA_CHECK (evtStore()->retrieve (jets,"HLT_AntiKt10LCTopoTrimmedPtFrac4SmallR20Jets_jes"));                                                                   
    for (const xAOD::Jet* jet : *jets) {                                                                                               
      hist ("h_gjetPt")->Fill (jet->pt() * 0.001); // GeV 
    }

  float  ptjetmax = 0.0;
  float  etajetmax = 0.0;
  float  phijetmax = 0.0;


    // loop over the jets in the container --- skip for now
   
  for (const xAOD::Jet *jet : *jets) {
      ANA_MSG_INFO ("execute(): jet pt = " << (jet->pt() * 0.001) << " GeV, phi " << (jet->phi()) << ", eta=" << (jet->eta()) ); // just to print out something
      if( (jet->pt() > ptjetmax ) && (jet->eta() > -2.5 ) && (jet->eta()<2.5) ){
          ptjetmax = jet->pt();
      }
   }   

  //Fill histograms
  // hist("h_gjetPt")->Fill(jet->pt())


 
  // const xAOD::gFexJetRoIContainer* gjets = nullptr;
  // ANA_CHECK (evtStore()->retrieve (gjets,"L1_gFexLRJetRoI"));




  // float  etgjetmax = 0.0;
  // float  etagjetmax = 0.0;
  // float  phigjetmax = 0.0;

  // for( const xAOD::gFexJetRoI *gjet : *gjets ){
  //    if( (gjet->et() > etgjetmax ) ){
  //           etgjetmax   = gjet->et();
  //           etagjetmax  = gjet->eta();
  //           phigjetmax  = gjet->phi();
  //    }
  // }
  // if ( etgjetmax > 0 ) {
  //          ANA_MSG_INFO ("g jet  max = " << (etgjetmax*0.001)  << "GeV, phi = " << (phigjetmax) << "rad, eta = " << (etagjetmax) );
  //          hist ("h_gjetPt")->Fill  (etgjetmax * 0.001); // GeV} 
  //          hist ("h_gjetPhi")->Fill (phigjetmax); // GeV}      
  //          hist ("h_gjetEta")->Fill (etagjetmax); // GeV}     
  // }

  tree("analysis")->Fill();

return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk. This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}




StatusCode MyxAODAnalysis::parseList(const std::string& line, std::vector<std::string>& result) const {
  //
  // Copied from AthMonitorAlgorithm - parse the supplied trigger chains
  //
  std::string item;
  std::stringstream ss(line);

  ANA_MSG_DEBUG( "L1CaloAODAnalysis::parseList()" );

  while ( std::getline(ss, item, ',') ) {
    std::stringstream iss(item); // remove whitespace
    iss >> item;
    result.push_back(item);
  }

  return StatusCode::SUCCESS;
}


