# See: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake for more details about anything here

# Specify local input file name
#testFile = os.getenv('ASG_TEST_FILE_MC')
testFile = '/data/jlue/data/EJ/Py8EG_Zprime2EJsAODs/valid1.801931.Py8EG_Zprime2EJs_Ld40_rho80_pi20_Zp600_l1.recon.AOD.e8514_e828_s4159_s4114_r15332/AOD.37358759._000001.pool.root.1'

jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:MyxAODAnalysis.outputs.root"]
svcMgr.THistSvc.MaxFileSize=-1 #speeds up jobs that output lots of histograms

# Override next line on command line with: --filesInput=XXX
jps.AthenaCommonFlags.FilesInput = [testFile] 

# Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "ClassAccess" 

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )

# Later on we'll add some configuration options for our algorithm that go here

from AnaAlgorithm.DualUseConfig import addPrivateTool
# add the TDT tool to the algorithm
addPrivateTool( alg, 'triggerDecTool', 'Trig::TrigDecisionTool' )
alg.TriggerChain="L1_gLJ140,L1_gJ100"


#// add trig decicion tool here https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/trig_decisiontool/
#https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/trig_analysis/

#xAODConfTool = createPublicTool( 'TrigConf::xAODConfigTool', 'xAODConfigTool' )
#job.algsAdd (xAODConfTool)
#addPrivateTool( alg, 'trigDecisionTool', 'Trig::TrigDecisionTool' )

#alg.trigDecisionTool.ConfigTool = '%s/%s' % ( xAODConfTool.getType(), xAODConfTool.getName() )
#alg.trigDecisionTool.TrigDecisionKey = "xTrigDecision"

# We need to explicitly instantiate CutFlowSvc in Athena
#from EventBookkeeperTools.CutFlowHelpers import CreateCutFlowSvc
#CreateCutFlowSvc( seq=athAlgSeq )

# Add our algorithm to the main alg sequence
athAlgSeq += alg

# Limit the number of events (for testing purposes)
theApp.EvtMax = 500



# Optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")


